# Ultrasonic Sensor Monitoring System

## Introduction

This project involves monitoring the presence of car using an ultrasonic sensor connected to an ESP8266 microcontroller. The SolidJS code displays the presence status on a web interface by fetching data from the ESP8266 via HTTP requests.

## Getting Started

### Prerequisites

- SolidJS
- ESP8266WiFi library
- ESP8266WebServer library

### Installation

1. Connect the ultrasonic sensor to the ESP8266 microcontroller following the pin definitions in the Arduino code.
2. Upload the Arduino code to the ESP8266 microcontroller.

### Running the Code

1. Start the ESP8266 microcontroller to establish a Wi-Fi connection.
2. Run the SolidJS code to display the monitoring interface:

   ```bash
   $ pnpm install
   $ pnpm dev
   ```

## Usage Examples

### Example 1: Monitoring Presence Status

Access the monitoring interface in a web browser:

- URL: `http://{IP-Address-of-ESP8266}:8080`

The interface displays two areas labeled "P1" and "P2", indicating the presence status of objects in those areas. The background color changes based on the presence status: green for presence detected and red for absence.

## Contributing

Contributions to this project are welcome. Please follow these guidelines:

1. Fork the repository.
2. Create a new branch for your feature (`git checkout -b feature-name`).
3. Make your changes.
4. Test your changes thoroughly.
5. Submit a pull request.

## License Information

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details.

## Contact Information

For any inquiries or issues, please contact theplantinthedesk at t19.thakur@outlook.com.
