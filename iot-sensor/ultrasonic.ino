#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>

// Pin definitions
const int trigPin = D1;     // Trigger pin of the ultrasonic sensor
const int echoPin = D2;     // Echo pin of the ultrasonic sensor
const int buzzerPin = D3;   // Buzzer pin

// WiFi credentials
const char* ssid = "ADMIN";        // Your SSID
const char* password = "qwertyui9";    // Your Password

// Global variables
unsigned long lastGetTimeMs = 0;
boolean initialRequestSent = false;
boolean present = false; // Initially set to false
float floor_height = 15.0; // Floor height set to 30 centimeters
String lastJsonResponse; // Variable to store the last JSON response

// Helper methods
void triggerBuzzer();
float getDistance();
String getJsonDistanceData(boolean present);

// Instantiate the Web Server
ESP8266WebServer server(80);

void setup() {
  pinMode(trigPin, OUTPUT);
  pinMode(echoPin, INPUT);
  pinMode(buzzerPin, OUTPUT);

  Serial.begin(115200);
  setupWiFi();

  // Define routes
  server.on("/", handleRoot);
  server.on("/api/distance", [](){
      if (!initialRequestSent || ((millis() - lastGetTimeMs) > 5000)) { // Only process the request if more than 5 seconds passed since the previous GET request
        float distance = getDistance();
        // Update present value based on conditions
        if (floor_height - distance > 6 && floor_height - distance < 8) {
          present = true;
        } else {
          present = false;
        }
        lastJsonResponse = getJsonDistanceData(present); // Store the JSON response
        server.send(200, "application/json", lastJsonResponse); // Send the stored JSON response
        lastGetTimeMs = millis();

        if (!initialRequestSent) { // Record the timestamp only once for future comparison
          initialRequestSent = true;
        }
      } else {
        server.send(200, "application/json", lastJsonResponse); // Send the stored JSON response
      }
    });

  server.begin();
  Serial.println("HTTP server started");
}

void loop() {
  server.handleClient();
  
  // Print IP address every 10 seconds
  static unsigned long lastPrintTime = 0;
  if (millis() - lastPrintTime > 10000) {
    Serial.print("IP address: ");
    Serial.println(WiFi.localIP());
    lastPrintTime = millis();
  }
}

void handleRoot() {
  String htmlResponse = "<html><head>"
                         "<title>ESP8266 Ultrasonic Sensor</title>"
                       "</head><body>"
                         "<h1>Ultrasonic Sensor - Local API Demo</h1>"
                         "<p>Visit &lt;http://" + WiFi.localIP().toString() + "/api/distance&gt; for current measurement.</p>"
                       "</body></html>";
  server.send(200, "text/html", htmlResponse);
}

float getDistance() {
  digitalWrite(trigPin, LOW);
  delayMicroseconds(2);
  digitalWrite(trigPin, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPin, LOW);

  unsigned long duration = pulseIn(echoPin, HIGH);
  float distance = duration * 0.034 / 2; // Convert microsecond to centimeter

  return roundf(distance * 10)/10; // Round to nearest tenth
}

String getJsonDistanceData(boolean present) {
  String jsonResponse = "{";
  jsonResponse += "\"present\": ";
  jsonResponse += present ? "true" : "false";
  jsonResponse += "}";
  return jsonResponse;
}

void setupWiFi() {
  Serial.println();
  Serial.println("Connecting to WiFi");

  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(1000);
    Serial.print(".");
  }

  Serial.println();
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}

void triggerBuzzer() {
  digitalWrite(buzzerPin, HIGH);
  delay(500);
  digitalWrite(buzzerPin, LOW);
  delay(500);
}
