import { createEffect, createSignal, onMount } from "solid-js";
import axios from "axios";

export default function Sensor() {
  const [presence, setPresence] = createSignal(false);

  onMount(() => {
    const fetchData = async () => {
      try {
        const response = await axios.get('http://<IP-Adress-Of-Sensor>:5000/api/distance');
        setPresence(response.data.present);
      } catch (error) {
        console.error(error);
      }
    }

    setInterval(fetchData, 5000);
  })

  return (
    <main class="w-screen h-screen bg-[url('/road.jpg')] bg-[length:1000px_500px]">
      <div class="absolute top-1/2 w-screen h-20 bg-yellow-600">

      </div>
      <div class="flex justify-center items-center h-full gap-4 ">
        <div class={`w-[32rem] h-[40rem] ${presence() ? 'bg-green-500' : 'bg-red-500'} flex justify-center items-center font-bold text-[16rem] border-[16px] border-solid border-white z-10`}> <span class="opacity-40">P1</span></div>
        <div class="w-[32rem] h-[40rem] flex justify-center items-center bg-gray-200 font-bold text-[16rem] border-[16px] border-solid border-white z-10"><span class="opacity-40">P2</span></div>
      </div>
    </main>
  );
}